import "./App.css";
import { useState } from "react";

import { MdAdd } from "react-icons/md";
import { SlTrash } from "react-icons/sl";
import { FaGitlab } from "react-icons/fa";

const TodoInput = ({ todo, setTodo, addTodo }) => (
  <div className="input-wrapper">
    <input
      type="text"
      id="todo-input"
      name="todo"
      value={todo}
      placeholder="What's next?"
      onChange={(e) => setTodo(e.target.value)}
      onKeyUp={(e) => {
        if (e.key === "Enter") {
          addTodo();
        }
      }}
    />
    <button className="add-button" onClick={addTodo}>
      <MdAdd size={21} />
    </button>
  </div>
);

const TodoList = ({ todoList, removeTodo }) => (
  <div className="todo-list-container">
    {todoList.length ? (
      <ul className="todo-list">
        {todoList.map((entry, index) => (
          <li key={index} className="todo-item">
            <span>{entry}</span>
            <button className="delete-button" onClick={() => removeTodo(entry)}>
              <SlTrash size={18} />
            </button>
          </li>
        ))}
      </ul>
    ) : null}
  </div>
);

const Footer = () => (
  <footer className="footer">
    <div className="empty">Add some tasks to get started!</div>
    <div className="social-icons">
      <a
        href="https://gitlab.com/Cipyar"
        rel="noreferrer"
        target="_blank"
        title="GitLab"
      >
        <FaGitlab size={25} />
      </a>
    </div>
  </footer>
);

const App = () => {
  const [todo, setTodo] = useState("");
  const [todos, setTodos] = useState([]);
  const [error, setError] = useState(null);
  const [darkMode, setDarkMode] = useState(false);

  const addTodo = () => {
    if (todo === "") {
      setError("You must enter a task to create one.");
      return;
    }
    setError(null);
    setTodos([...todos, todo]);
    setTodo("");
  };

  const deleteTodo = (task) => {
    const newTodos = todos.filter((t) => t !== task);
    setTodos(newTodos);
  };

  const toggleDarkMode = () => {
    setDarkMode((prev) => !prev);
  };

  return (
    <div className={`App ${darkMode ? "dark-mode" : ""}`}>
      <div className="dark-mode-section">
        <span>{darkMode ? "Toggle Dark mode off" : "Toggle Dark mode on"}</span>
        <button className="dark-mode-toggle" onClick={toggleDarkMode}>
          <div className={darkMode ? "toggle-ball active" : "toggle-ball"}></div>
        </button>
      </div>
      <h1>Todo App</h1>
      {error && <div className="error-message">{error}</div>}
      <div className="content">
        <TodoInput todo={todo} setTodo={setTodo} addTodo={addTodo} />
        <TodoList todoList={todos} removeTodo={deleteTodo} />
      </div>
      <Footer />
    </div>
  );
};
  export default App;
  